import numpy as np
import scipy.stats

nn_q_id_file = open('my_metrics_res/sequential/nn_q_id.txt', 'r')
nn_real_file = open('my_metrics_res/sequential/nn_real.txt', 'r')
nn_sim_file = open('my_metrics_res/sequential/nn_simulated.txt', 'r')


array_query_id_to_real_clicks_arr = np.zeros((100000000, 10))
array_query_id_to_weight = np.zeros((100000000,))

array_query_id_to_simulated_clicks_arr = np.zeros((100000000, 10))

array_query_id_to_first_simple_clicks_arr = np.zeros((100000000, 10))
array_query_id_to_second_simple_clicks_arr = np.zeros((100000000, 10))

second_simulator = np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0])

line_num = 1
skip_line_cnt = 0

while True:
    line_with_q_id = nn_q_id_file.readline()
    if not line_with_q_id:
        break

    q_id = int(float(line_with_q_id.strip()))

    line_with_real = nn_real_file.readline().strip()
    real_arr = np.fromstring(line_with_real, dtype=np.float64, sep=',')

    line_with_sim = nn_sim_file.readline().strip()
    sim_arr = np.fromstring(line_with_sim, dtype=np.float64, sep=',')

    r1 = real_arr.shape[0]
    s1 = sim_arr.shape[0]

    if r1 != 10 or s1 != 10:
        print(real_arr.shape, sim_arr.shape, 'Line', '#' + str(line_num))
        line_num += 1
        skip_line_cnt += 1
        continue

    array_query_id_to_real_clicks_arr[q_id] += real_arr
    array_query_id_to_simulated_clicks_arr[q_id] += sim_arr

    array_query_id_to_second_simple_clicks_arr[q_id] += second_simulator

    array_query_id_to_weight[q_id] += 1

    line_num += 1

print('skiped line cnt:', skip_line_cnt, 'from', line_num)
print('end reading info from files -> start calculate local KL_div')
q_ids = np.loadtxt('my_metrics_res/sequential/nn_q_id.txt', dtype=np.float64, delimiter=',')
sum_div_sim = 0
sum_div_first = 0
sum_div_second = 0
n = 0

q_ids = np.unique(q_ids)
print('Number of unique query:', len(q_ids))

print('calculate local KL-div by Session or Rank???')

for q_id in q_ids:
    q_id = int(q_id)
    p = array_query_id_to_real_clicks_arr[q_id]
    q1 = array_query_id_to_simulated_clicks_arr[q_id]
    w = array_query_id_to_weight[q_id]

    p[p == 0] = 1e-5
    q1[q1 == 0] = 1e-5

    div_sim = scipy.stats.entropy(p, q1)

    sum_div_sim += div_sim * w

    #### first simple
    q2 = array_query_id_to_first_simple_clicks_arr[q_id]

    q2[q2 == 0] = 1e-5

    div_first = scipy.stats.entropy(p, q2)

    sum_div_first += div_first * w

    #### second simple
    q3 = array_query_id_to_second_simple_clicks_arr[q_id]

    q3[q3 == 0] = 1e-5

    div_second = scipy.stats.entropy(p, q3)

    sum_div_second += div_second * w

    n += w

print('n', n)
print('Mean local KL div simulated', sum_div_sim / n)
print('Mean local KL div first', sum_div_first / n)
print('Mean local KL div second', sum_div_second / n)




