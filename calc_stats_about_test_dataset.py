import numpy as np
import re

general_file = open('my_metrics_res_on_40/sequential/nn_q_id@@@real@@@simulated.txt', 'r')

all_zero_cnt = 0
just_1_click_in_fst_pos_cnt = 0
non_click_in_fst_pos_cnt = 0
rest_cnt = 0

line_num = 1
for line in general_file:
    line = line.strip()
    elements = line.split('@@@')
    str_real_arr = re.sub('[\[\]]', '', elements[1])
    real_arr = np.fromstring(str_real_arr, dtype=float, sep=' ')

    if np.all(real_arr == 0):
        all_zero_cnt += 1

    elif real_arr[0] == 1 and np.all(real_arr[1:] == 0):
        just_1_click_in_fst_pos_cnt += 1

    elif real_arr[0] == 0:
        non_click_in_fst_pos_cnt += 1

    else:
        rest_cnt += 1

    line_num += 1

percent_0 = float(all_zero_cnt) / line_num
percent_1 = float(just_1_click_in_fst_pos_cnt) / line_num
percent_2 = float(non_click_in_fst_pos_cnt) / line_num
percent_rest = float(rest_cnt) / line_num
print('% of sessions with 0 clicks:', percent_0)
print('% of sessions with only 1 click on first position:', percent_1)
print('% of sessions with only non click on first position:', percent_2)
print('% of rest:', percent_rest)

print('% of sum:', percent_0 + percent_1 + percent_rest)
