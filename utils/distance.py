import numpy as np

class Distance:

    def __init__(self, model_type="sequential", sequence_length=3):
        self.model_type = model_type
        self.sequence_length = sequence_length

    def calculate_distance(self, candidate, target):
        candidate = np.asarray(candidate).astype(float)
        target = np.asarray(target).astype(float)
        if self.model_type == 'sequential':
            candidate =  self.preprocess2(candidate)
        else:
            candidate =  self.preprocess(candidate)
        target = self.preprocess(target)
        distance = self.weighted_DL(candidate,target)
        lcs = self.lcs(target,candidate)
        return distance, lcs

    # Creates ordered clicks from 10x11 click sequence
    def preprocess(self, clicks):
        if len(np.where(clicks==1.0)) > 1:
            clicks = np.where(clicks == 1.0)[1] + 1
            clicks = clicks[clicks<11].tolist()
        else:
            clicks = []
        return clicks

        # Creates ordered clicks from 1x10 click sequence (for sequential prediction)
    def preprocess2(self,clicks):
        clicks = np.where(clicks == 1.0)[0] + 1
        clicks = clicks.tolist()
        return clicks

    # Original code from https://rosettacode.org/wiki/Longest_common_subsequence#Python
    def lcs(self,a, b):
        lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
        # row 0 and column 0 are initialized to 0 already
        for i, x in enumerate(a):
            for j, y in enumerate(b):
                if x == y:
                    lengths[i+1][j+1] = lengths[i][j] + 1
                else:
                    lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
        # read the substring out from the matrix
        result = []
        x, y = len(a), len(b)
        while x != 0 and y != 0:
            if lengths[x][y] == lengths[x-1][y]:
                x -= 1
            elif lengths[x][y] == lengths[x][y-1]:
                y -= 1
            else:
                assert a[x-1] == b[y-1]
                result = [a[x-1]] + result
                x -= 1
                y -= 1
        if len(a) > 0:
            lcs = float(len(result))/max(len(a),len(b))
        else:
            lcs = 1.0/(len(b)+1)
        return lcs

    # Original code from http://softwareengineering.stackexchange.com/questions/194136/possible-damerau-levenshtein-improvement
    def weighted_DL(self, a, b):
        INF = len(a) + len(b)

        # Matrix: (M + 2) x (N + 2)
        matrix  = [[INF for n in xrange(len(b) + 2)]]
        matrix += [[INF] + range(len(b) + 1)]
        matrix += [[INF, m] + [0] * len(b) for m in xrange(1, len(a) + 1)]

        # Holds last row each element was encountered: `DA` in the Wikipedia pseudocode
        last_row = {}

        # Fill in costs
        for row in xrange(1, len(a) + 1):
            # Current character in `a`
            ch_a = a[row-1]

            # Column of last match on this row: `DB` in pseudocode
            last_match_col = 0

            for col in xrange(1, len(b) + 1):
                # Current character in `b`
                ch_b = b[col-1]

                # Last row with matching character; `i1` in pseudocode
                last_matching_row = last_row.get(ch_b, 0)

                # Cost of substitution
                cost = 0 if ch_a == ch_b else 1

                # Compute substring distance
                matrix[row+1][col+1] = min(
                    matrix[row][col] + cost, # Substitution
                    matrix[row+1][col] + 1,  # Addition
                    matrix[row][col+1] + 1,  # Deletion

                    # Transposition
                    matrix[last_matching_row][last_match_col]
                        + (row - last_matching_row - 1) + 0.75
                        + (col - last_match_col - 1))

                # If there was a match, update last_match_col
                # Doing this here lets me be rid of the `j1` variable from the original pseudocode
                if cost == 0:
                    last_match_col = col

            # Update last row for current character
            last_row[ch_a] = row
        distance = matrix[-1][-1]
        # Return last element
        return distance
