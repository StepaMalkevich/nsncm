import numpy as np
import re

general_file = open('my_metrics_res_on_40/sequential/nn_q_id@@@real@@@simulated.txt', 'r')

real_distr = np.zeros((10,))
simulated_distr = np.zeros((10,))

real_number_of_clicks_distr = np.zeros((11,))
sim_number_of_clicks_distr = np.zeros((11,))

line_num = 1
for line in general_file:
    line = line.strip()
    elements = line.split('@@@')
    q_id = int(elements[0])
    str_real_arr = re.sub('[\[\]]', '', elements[1])
    str_simulated_arr = re.sub('[\[\]]', '', elements[2])

    real_arr = np.fromstring(str_real_arr, dtype=float, sep=' ')
    sim_arr = np.fromstring(str_simulated_arr, dtype=float, sep=' ')

    real_distr += real_arr
    simulated_distr += sim_arr

    real_number_of_clicks = np.sum(real_arr)
    real_number_of_clicks_distr[(int)(real_number_of_clicks)] += 1

    sim_number_of_clicks = np.sum(sim_arr)
    sim_number_of_clicks_distr[(int)( sim_number_of_clicks)] += 1

print('Positions of clicks\n')
print('real', real_distr)
print('simulated', simulated_distr)

print('Number of clicks\n')
print('real', real_number_of_clicks_distr)
print('simulated', sim_number_of_clicks_distr)