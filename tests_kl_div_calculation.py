import numpy as np
import scipy.stats

p = np.zeros((10, ))
q = np.zeros((10, ))
p[1] = 1
p[2] = 1
p[3] = 1

q[0] = 1

print(p)
print(q)

p[p == 0] = 1e-5
q[q == 0] = 1e-5

div_sim = scipy.stats.entropy(p, q)

print(div_sim)