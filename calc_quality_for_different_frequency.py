import numpy as np

nn_diff_first_rank_file = open('my_metrics_res_on_40/sequential/nn_diff_first_rank.txt', 'r')
nn_diff_second_rank_file = open('my_metrics_res_on_40/sequential/nn_diff_second_rank.txt', 'r')

first_diff_first_rank_file = open('my_metrics_res_on_40/simple/first/first_diff_first_rank.txt', 'r')
first_diff_second_rank_file = open('my_metrics_res_on_40/simple/first/first_diff_second_rank.txt', 'r')

second_diff_first_rank_file = open('my_metrics_res_on_40/simple/second/second_diff_first_rank.txt', 'r')
second_diff_second_rank_file = open('my_metrics_res_on_40/simple/second/second_diff_second_rank.txt', 'r')

q_id_file = open('my_metrics_res_on_40/sequential/nn_q_id.txt', 'r')

weight_q_id = np.zeros((100000000,))

i = 0
for q_id_line in q_id_file:
    q_id_line = q_id_line.strip()
    if not q_id_line:
        break

    q_id = int(float(q_id_line))
    weight_q_id[q_id] += 1
    i += 1

print('i', i)

q_id_file.close()

q_id_file = open('my_metrics_res_on_40/sequential/nn_q_id.txt', 'r')

# neural
rare_query_nn_diff_first_rank = 0
torso_query_nn_diff_first_rank = 0
frequent_query_nn_diff_first_rank = 0

rare_query_nn_diff_second_rank = 0
torso_query_nn_diff_second_rank = 0
frequent_query_nn_diff_second_rank = 0

# first simple
rare_query_first_diff_first_rank = 0
torso_query_first_diff_first_rank = 0
frequent_query_first_diff_first_rank = 0

rare_query_first_diff_second_rank = 0
torso_query_first_diff_second_rank = 0
frequent_query_first_diff_second_rank = 0

# second simple
rare_query_second_diff_first_rank = 0
torso_query_second_diff_first_rank = 0
frequent_query_second_diff_first_rank = 0

rare_query_second_diff_second_rank = 0
torso_query_second_diff_second_rank = 0
frequent_query_second_diff_second_rank = 0

n_rare = 0
n_torso = 0
n_frequent = 0

i = 0
while True:
    q_id_line = q_id_file.readline().strip()

    if not q_id_line:
        break

    q_id = int(float(q_id_line))
    nn_diff_first_rank = float(nn_diff_first_rank_file.readline().strip())
    nn_diff_second_rank = float(nn_diff_second_rank_file.readline().strip())

    first_diff_first_rank = float(first_diff_first_rank_file.readline().strip())
    first_diff_second_rank = float(first_diff_second_rank_file.readline().strip())

    second_diff_first_rank = float(second_diff_first_rank_file.readline().strip())
    second_diff_second_rank = float(second_diff_second_rank_file.readline().strip())

    if (weight_q_id[q_id] >= 1) and (weight_q_id[q_id] < 10):
        rare_query_nn_diff_first_rank += nn_diff_first_rank
        rare_query_nn_diff_second_rank += nn_diff_second_rank

        rare_query_first_diff_first_rank += first_diff_first_rank
        rare_query_first_diff_second_rank += first_diff_second_rank

        rare_query_second_diff_first_rank += second_diff_first_rank
        rare_query_second_diff_second_rank += second_diff_second_rank

        n_rare += 1

    if (weight_q_id[q_id] >= 10) and (weight_q_id[q_id] < 100):
        torso_query_nn_diff_first_rank += nn_diff_first_rank
        torso_query_nn_diff_second_rank += nn_diff_second_rank

        torso_query_first_diff_first_rank += first_diff_first_rank
        torso_query_first_diff_second_rank += first_diff_second_rank

        torso_query_second_diff_first_rank += second_diff_first_rank
        torso_query_second_diff_second_rank += second_diff_second_rank

        n_torso += 1

    if weight_q_id[q_id] >= 100:
        frequent_query_nn_diff_first_rank += nn_diff_first_rank
        frequent_query_nn_diff_second_rank += nn_diff_second_rank

        frequent_query_first_diff_first_rank += first_diff_first_rank
        frequent_query_first_diff_second_rank += first_diff_second_rank

        frequent_query_second_diff_first_rank += second_diff_first_rank
        frequent_query_second_diff_second_rank += second_diff_second_rank

        n_frequent += 1

    i += 1

print('i', i)


print("Number of rare queries:", n_rare)
print("Number of torso queries:", n_torso)
print("Number of frequent queries:", n_frequent)

print('----------------------')
print('MEA for nn of diff first rank for rare queries', rare_query_nn_diff_first_rank / n_rare)
print('MEA for nn of diff second rank for rare queries', rare_query_nn_diff_second_rank / n_rare)

print('MEA for nn of diff first rank for torso queries', torso_query_nn_diff_first_rank / n_torso)
print('MEA for nn of diff second rank for torso queries', torso_query_nn_diff_second_rank / n_torso)

print('MEA for nn of diff first rank for frequent queries', frequent_query_nn_diff_first_rank / n_frequent)
print('MEA for nn of diff second rank for frequent queries', frequent_query_nn_diff_second_rank / n_frequent)
print('----------------------')

print('MEA for first of diff first rank for rare queries', rare_query_first_diff_first_rank / n_rare)
print('MEA for first of diff second rank for rare queries', rare_query_first_diff_second_rank / n_rare)

print('MEA for first of diff first rank for torso queries', torso_query_first_diff_first_rank / n_torso)
print('MEA for first of diff second rank for torso queries', torso_query_first_diff_second_rank / n_torso)

print('MEA for first of diff first rank for frequent queries', frequent_query_first_diff_first_rank / n_frequent)
print('MEA for first of diff second rank for frequent queries', frequent_query_first_diff_second_rank / n_frequent)
print('----------------------')
print('MEA for second of diff first rank for rare queries', rare_query_second_diff_first_rank / n_rare)
print('MEA for second of diff second rank for rare queries', rare_query_second_diff_second_rank / n_rare)

print('MEA for second of diff first rank for torso queries', torso_query_second_diff_first_rank / n_torso)
print('MEA for second of diff second rank for torso queries', torso_query_second_diff_second_rank / n_torso)

print('MEA for second of diff first rank for frequent queries', frequent_query_second_diff_first_rank / n_frequent)
print('MEA for second of diff second rank for frequent queries', frequent_query_second_diff_second_rank / n_frequent)