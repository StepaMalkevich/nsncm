import os
import sys
sys.path.insert(0,'utils')
sys.path.insert(0,'models/sequential')
#sys.path.insert(0,'models/attention_doc_context')
sys.path.insert(0,'models/non_sequential_attention')
sys.path.insert(0,'models/non_sequential')
import datetime
import numpy as np
import tensorflow as tf
import numpy_indexed as npi

from metrics import Metrics
from batches import Batches
import argparse
import json
from operator import itemgetter
import time
import itertools
import threading

from results import Results
from sequential import NCM
from attention import NS_Attention
from non_sequential import NS_NCM
from config import *
import shutil

def thread_data(model_type, click_model, batches, batch_data):
    t = time.time()
    q_id, s_id, batch_queries, batch_docs, batch_seq_labels, batch_nonseq_labels  = batches.create_batch()
    t_get = time.time() - t
    t = time.time()
    if model_type == "sequential":
        data = click_model.create_data(batch_queries, batch_docs, batch_seq_labels)
        t_create = time.time()-t
        batch_data['labels'] = batch_seq_labels
        batch_data['full_labels'] = batch_nonseq_labels
    else:
        data = click_model.create_data(batch_queries, batch_docs)
        t_create = time.time()-t
        batch_data['labels'] = batch_nonseq_labels
        batch_data['full_labels'] = batch_nonseq_labels
    batch_data['q_id'] = q_id
    batch_data['s_id'] = s_id
    batch_data['q'] = batch_queries
    batch_data['d'] = batch_docs
    batch_data['data'] = data
    return batch_data

def thread_create_data(model_type, click_model, batch_queries, batch_docs, batch_labels, batch_data):
    t = time.time()
    if model_type == "sequential":
        data = click_model.create_data(batch_queries, batch_docs, batch_labels)
    else:
        data = click_model.create_data(batch_queries, batch_docs)
    batch_data['data'] = data
    batch_data['labels'] = batch_labels
    batch_data['t'] = time.time() - t

def train_op(sess, click_model, batch_data, batch_labels, batch_loss):
    t = time.time()
    l = click_model.train(sess, batch_data, batch_labels)
    batch_loss.append(l)
    batch_loss.append(time.time()-t)

def get_min_max_click_pos(clicks):
    is_click = np.isin(clicks, [1])
    is_arr_without_any_click = np.any(is_click, axis=1)
    pos_of_such_arrs = np.argwhere(is_arr_without_any_click == False)

    default_val = np.full((pos_of_such_arrs.shape[0]), -1)
    arr_pos_with_zero_rank_click = np.column_stack((pos_of_such_arrs, default_val))
    position_of_clicks = np.argwhere(is_click == True)
    all_position_of_clicks = np.concatenate((position_of_clicks, arr_pos_with_zero_rank_click), axis=0)

    min_click_pos = npi.group_by(all_position_of_clicks[:, 0]).min(all_position_of_clicks)[1]
    max_click_pos = npi.group_by(all_position_of_clicks[:, 0]).max(all_position_of_clicks)[1]

    return min_click_pos, max_click_pos

def get_diff_first_last_click_pos(real, pred):
    min_click_pos_real, max_click_pos_real = get_min_max_click_pos(real)
    min_click_pos_pred, max_click_pos_pred = get_min_max_click_pos(pred)

    diff_first_rank_clicks = np.abs(min_click_pos_real - min_click_pos_pred)
    diff_second_rank_clicks = np.abs(max_click_pos_real - max_click_pos_pred)

    is_all_fst_col_zero = np.all(diff_first_rank_clicks[:, 0] == 0)
    assert is_all_fst_col_zero, 'not all first col is zero => error in group by'

    is_all_fst_col_zero = np.all(diff_second_rank_clicks[:, 0] == 0)
    assert is_all_fst_col_zero, 'not all first col is zero => error in group by'

    diff_first_rank = diff_first_rank_clicks[:, 1]
    diff_second_rank = diff_second_rank_clicks[:, 1]

    return diff_first_rank, diff_second_rank

def write_to_file(arr, filename):
    f1 = open(filename, 'ab')
    np.savetxt(f1, arr, delimiter=',')

def validate_op_for_KL_2(root_dir, sess, click_model, batch_data, batch_labels, batch_query_ids, batch_loss):
    predictions = []
    prediction_op(sess, click_model, batch_data, batch_labels, predictions)

    # simulated model
    simulate_clicks = np.vectorize(lambda p: np.random.binomial(1, p, 1)[0])
    simulated_clicks = simulate_clicks(predictions[0])
    simulated_clicks = simulated_clicks.astype(float)

    num = len(batch_query_ids)
    res = ''
    for i in range(num):
        line = str(batch_query_ids[i]) + '@@@' + str(batch_labels[i]) + '@@@' + str(simulated_clicks[i]) + '\n'
        res += line

    res_file = open(root_dir + 'sequential/nn_q_id@@@real@@@simulated.txt', 'ab')
    res_file.write(res)

def validate_op_for_KL(sess, click_model, batch_data, batch_labels, batch_query_ids, batch_loss):
    raise TypeError('Deprecated method')

    predictions = []
    prediction_op(sess, click_model, batch_data, batch_labels, predictions)

    # simulated model
    simulate_clicks = np.vectorize(lambda p: np.random.binomial(1, p, 1)[0])
    simulated_clicks = simulate_clicks(predictions[0])

    f1 = open('my_metrics_res/sequential/nn_q_id.txt', 'ab')
    np.savetxt(f1, batch_query_ids, delimiter=',')

    f2 = open('my_metrics_res/sequential/nn_simulated.txt', 'ab')
    np.savetxt(f2, simulated_clicks, delimiter=',')

    f3 = open('my_metrics_res/sequential/nn_real.txt', 'ab')
    np.savetxt(f3, batch_labels, delimiter=',')


def validate_op(root_dir, sess, click_model, batch_data, batch_labels, batch_query_ids, batch_loss):
    # p = click_model.get_predictions(sess, batch_data, batch_labels)
    # t = time.time()

    predictions = []
    prediction_op(sess, click_model, batch_data, batch_labels, predictions)

    # simulated model
    simulate_clicks = np.vectorize(lambda p: np.random.binomial(1, p, 1)[0])
    simulated_clicks = simulate_clicks(predictions[0])

    diff_first_rank, diff_second_rank = get_diff_first_last_click_pos(simulated_clicks, batch_labels)
    f1 = open(root_dir + 'sequential/nn_diff_first_rank.txt', 'ab')
    np.savetxt(f1, diff_first_rank, delimiter=',')

    f2 = open(root_dir + 'sequential/nn_diff_second_rank.txt', 'ab')
    np.savetxt(f2, diff_second_rank, delimiter=',')

    # first simple model
    first_simple_clicks = np.zeros_like(batch_labels)
    first_diff_first_rank, first_diff_second_rank = get_diff_first_last_click_pos(first_simple_clicks, batch_labels)

    first_simple_first_file_name = root_dir + 'simple/first/first_diff_first_rank.txt'
    write_to_file(first_diff_first_rank, first_simple_first_file_name)

    first_simple_second_file_name = root_dir + 'simple/first/first_diff_second_rank.txt'
    write_to_file(first_diff_second_rank, first_simple_second_file_name)

    # second simple model
    second_simple_clicks = np.zeros_like(batch_labels)
    second_simple_clicks[:, 0] = 1
    second_diff_first_rank, second_diff_second_rank = get_diff_first_last_click_pos(second_simple_clicks, batch_labels)

    second_simple_first_file_name = root_dir + 'simple/second/second_diff_first_rank.txt'
    write_to_file(second_diff_first_rank, second_simple_first_file_name)

    second_simple_second_file_name = root_dir + 'simple/second/second_diff_second_rank.txt'
    write_to_file(second_diff_second_rank, second_simple_second_file_name)


    # [loss,_] = click_model.get_loss(sess, batch_data, batch_labels)
    # batch_loss.append(loss)
    # batch_loss.append(time.time()-t)

def prediction_op(sess, click_model, batch_data, batch_labels, predictions):
    predictions.append(click_model.get_predictions( sess, batch_data, batch_labels))

def train( model_type, model):
    print model_type
    if not os.path.isdir(LOG_DIR + model_type + "/"):
        os.makedirs(LOG_DIR + model_type + "/")
    if not os.path.isdir(CHECKPOINT_DIR + model_type + "/"):
        os.makedirs(CHECKPOINT_DIR + model_type + "/")
    if not os.path.isdir(DATA_DIR):
        print("Please specify a correct data folder.")
        sys.exit()

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = False
    with tf.Session(config=config) as sess:
        if model_type == "sequential":
            click_model = NCM(BATCH_SIZE,LSTM_SIZE, max_saved = MAX_TO_KEEP)
        elif model_type == "non_sequential":
            click_model = NS_NCM(BATCH_SIZE, LSTM_SIZE, FINAL_LSTM_SIZE,max_saved = MAX_TO_KEEP)
        elif model_type == "attention":
            print "attention"
            click_model = NS_Attention(BATCH_SIZE, ATTENTION_LSTM, ATTENTION_FINAL_LSTM,max_saved = MAX_TO_KEEP)
        else:
            print("Please enter --model_type sequential or --model_type non_sequential or --model_type attention")
            sys.exit()
        if DATA_DIR == None:
            print( "No data files found")
            sys.exit()

        timestamp = "/" + datetime.datetime.now().strftime('%b-%d-%I%M%p-%G')
        train_writer = tf.summary.FileWriter(LOG_DIR + model_type + "/" + timestamp,sess.graph)
        if model == None:
            print( "\nInitializing variables")
            click_model.initialize(sess)
            save_id = 1
            begin = 0
        else:
            print("\nRestoring variables")
            save_id, begin = restore(click_model, model, sess)
            print "Save id: ", save_id, 'begin', begin

        # validate(sess, click_model, train_writer, model_type, 0)

        for j in range(begin,NUM_TRAIN_FILES):
            data = DATA_DIR + "train_query_sessions_with_behavioral_features.part_" + str(j) + ".gz"

            print( "Using data: ", data)
            batches = Batches(data, BATCH_SIZE)

            t = time.time()
            mean_loss = 0
            batch_data = {}
            thread_data(model_type,click_model,batches,batch_data)
            for i in range(ITERATIONS-1):
                batch_loss = []
                t2 = threading.Thread(target=train_op, args=(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss))
                batch_data = {}
                t1 = threading.Thread(target=thread_data, args=(model_type, click_model, batches, batch_data))
                t1.start()
                t2.start()
                t1.join()
                t2.join()
                mean_loss += batch_loss[0]
                if PRINT_PROGRESS == True and (i%PROGRESS_FREQUENCY == 0 or i == ITERATIONS-2):
                    p = (float(i)/ITERATIONS) * PROGRESS_FREQUENCY
                    print("Training: %.2f%%, loss: %.3f, time: %.2f, " % (p, mean_loss/PROGRESS_FREQUENCY, time.time()-t))
                    t = time.time()
                    mean_loss = 0
            batch_loss = []
            train_op(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss)
            print "saving"
            save = CHECKPOINT_DIR + model_type + "/" + "model_"+ str(save_id) +".ckpt"
            click_model.save_model(sess, save)
            save_id += 1
            batches.close_file()
            # validate(sess, click_model, train_writer, model_type, j+1)

def restore(click_model, model, sess):
    click_model.load_model(sess, model)
    begin = int(model.split("_")[-1].strip(".ckpt"))
    save_id = begin + 1
    return save_id, begin

def validate(sess, click_model, train_writer, model_type, j):
    data = DATA_DIR + VALIDATION_FILE
    print('data', data)
    batches = Batches(data, BATCH_SIZE)
    loss = 0.0
    t = time.time()

    batch_data = {}
    thread_data(model_type,click_model,batches,batch_data)
    # if j > 0:
    #     num_iterations = VALIDATION_ITERATIONS
    # else:
    #     num_iterations = 1
    num_iterations = VALIDATION_ITERATIONS
    for i in xrange(num_iterations-1):
        batch_loss = []
        t2 = threading.Thread(target=validate_op, args=(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss))
        batch_data = {}
        t1 =  threading.Thread(target=thread_data, args=(model_type, click_model, batches, batch_data))
        t1.start()
        t2.start()
        t1.join()
        t2.join()
        loss += np.mean(batch_loss[0])
        if PRINT_PROGRESS == True and (i%PROGRESS_FREQUENCY == 0 or i == num_iterations-2):
            p = (float(i)/num_iterations) * PROGRESS_FREQUENCY
            print("Testing: %.2f%%, loss: %.3f, time: %.2f" % (p, loss/(i+1), time.time()-t))
            t = time.time()
    batch_loss = []
    validate_op(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss)
    loss += np.mean(batch_loss[0])
    mean_loss = loss/num_iterations
    summary = click_model.add_summaries(sess, mean_loss)
    train_writer.add_summary(summary,j)
    batches.close_file()

def test(model_type, model, root_dir):
    print model_type
    if not os.path.isdir(OUTPUT_DIR + model_type + "/"):
        os.makedirs(OUTPUT_DIR + model_type + "/")

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=False
    sess = tf.Session(config=config)
    with tf.Session() as sess:
        if model_type == "sequential":
            click_model = NCM(TEST_SIZE,LSTM_SIZE)
        elif model_type == "non_sequential":
            click_model = NS_NCM(TEST_SIZE, LSTM_SIZE, FINAL_LSTM_SIZE)
        elif model_type == "attention":
            click_model = NS_Attention(TEST_SIZE, ATTENTION_LSTM, ATTENTION_FINAL_LSTM)
        else:
            print("Please enter --model_type sequential or --model_type non_sequential or --model_type attention")
            sys.exit()
        if model == None:
            click_model.initialize(sess)
        else:
            click_model.load_model(sess,model)

        for i in range(40):
            test_file = "test_query_sessions_with_behavioral_features.part_" + str(i) +".gz"
            data_file = DATA_DIR + test_file
            batches = Batches(data_file, TEST_SIZE, ratio=RATIO)
            print(test_file)

            for val_iter in xrange(TEST_ITERATIONS):
                if val_iter % 100 == 0:
                    print('val_iter:', val_iter)

                batch_data ={}
                thread_data(model_type,click_model,batches,batch_data)
                batch_loss = []

                # query_ids
                f1 = open(root_dir + 'sequential/nn_q_id.txt', 'ab')
                np.savetxt(f1, batch_data['q_id'], delimiter=',')

                # validate_op(root_dir, sess, click_model, batch_data['data'], batch_data['labels'], batch_data['q_id'], batch_loss)
                # validate_op_for_KL_2(root_dir, sess, click_model, batch_data['data'], batch_data['labels'], batch_data['q_id'], batch_loss)

            batches.close_file()

def recip_recall(metrics, best_predictions, labels, outcome):
    rr = metrics.reciprocal_rank(best_predictions,labels)
    recall = metrics.recall_at_r(rr)
    outcome.append(rr)
    outcome.append(recall)

def get_dcg(metrics, best_predictions, labels, outcome):
    d_dcg,lcs_dcg = metrics.calculate_dcgs(best_predictions, labels)
    outcome.append(d_dcg)
    outcome.append(lcs_dcg)

def get_best_n(scores, n, sequences):
    sequences = np.squeeze(sequences[:,0,:])
    best_predictions = []
    for val in scores:
        best = sorted(scores[val].items(), key=itemgetter(1),reverse=True)
        best_arrays = []
        for i in xrange(n):
            best_arrays.append(sequences[best[i][0]])
        best_predictions.append(best_arrays)
    return np.asarray(best_predictions)

def pretty_print(out_file, perpl):
    out_file.write("-------------\n")
    for i in xrange(len(perpl)):
        out_file.write("| %d | %.3f |\n" % (i+1, perpl[i]))
    out_file.write("-------------\n")

print("before main")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run Sequential LSTM')
    parser.add_argument('--model_type', type=str, default="sequential", help="Model type")
    parser.add_argument('--operation', type=str, default = 'test', help='Whether to train or to test')
    # CHECKPOINT_DIR + 'sequential' + "/" + "model_"+ str(2) +".ckpt"
    parser.add_argument('--model', type=str, default=CHECKPOINT_DIR + 'sequential' + "/" + "model_"+ str(40) +".ckpt", help='Saved pre-trained model from earlier training')
    args = parser.parse_args()

    print('args', args)

    if args.operation == 'train':
        train(args.model_type, args.model)
    elif args.operation == 'test':
        # todo: think about the way not to remove previous result
        root_dir = 'my_metrics_res_on_40/'

        # shutil.rmtree(root_dir)
        # os.makedirs(root_dir)
        # os.makedirs(root_dir + 'sequential')
        # os.makedirs(root_dir + 'simple')
        # os.makedirs(root_dir + 'simple/first')
        # os.makedirs(root_dir + 'simple/second')

        test(args.model_type, args.model, root_dir)
    else:
        print( "Please use --operation train or --operation test to specify the operation")


