import numpy as np

nn_diff_first_rank = np.loadtxt('my_metrics_res_on_40/sequential/nn_diff_first_rank.txt', delimiter=',')
nn_diff_second_rank = np.loadtxt('my_metrics_res_on_40/sequential/nn_diff_second_rank.txt', delimiter=',')

print('nn_diff_first_rank Mean', np.mean(nn_diff_first_rank))
print('nn_diff_second_rank Mean', np.mean(nn_diff_second_rank))
print('\n')

first_diff_first_rank = np.loadtxt('my_metrics_res_on_40/simple/first/first_diff_first_rank.txt', delimiter=',')
first_diff_second_rank = np.loadtxt('my_metrics_res_on_40/simple/first/first_diff_second_rank.txt', delimiter=',')

print('first_diff_first_rank Mean', np.mean(first_diff_first_rank))
print('first_diff_second_rank  Mean', np.mean(first_diff_second_rank))
print('\n')

second_diff_first_rank = np.loadtxt('my_metrics_res_on_40/simple/second/second_diff_first_rank.txt', delimiter=',')
second_diff_second_rank = np.loadtxt('my_metrics_res_on_40/simple/second/second_diff_second_rank.txt', delimiter=',')

print('second_diff_first_rank Mean', np.mean(second_diff_first_rank))
print('second_diff_second_rank  Mean', np.mean(second_diff_second_rank))