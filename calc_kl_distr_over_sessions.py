import numpy as np
import re
import scipy.stats

general_file = open('my_metrics_res_on_40/sequential/nn_q_id@@@real@@@simulated.txt', 'r')

array_query_id_to_real_clicks_arr = np.zeros((100000000, 11))
array_query_id_to_weight = np.zeros((100000000,))

array_query_id_to_simulated_clicks_arr = np.zeros((100000000, 11))
q_id_to_count = np.zeros((100000000,))

array_query_id_to_first_simple_clicks_arr = np.zeros((100000000, 11))
array_query_id_to_second_simple_clicks_arr = np.zeros((100000000, 11))

line_num = 1
for line in general_file:
    line = line.strip()
    elements = line.split('@@@')
    q_id = int(elements[0])
    str_real_arr = re.sub('[\[\]]', '', elements[1])
    str_simulated_arr = re.sub('[\[\]]', '', elements[2])

    real_arr = np.fromstring(str_real_arr, dtype=float, sep=' ')
    sim_arr = np.fromstring(str_simulated_arr, dtype=float, sep=' ')
    # q_ids must be unique
    q_id_to_count[q_id] += 1

    r1 = real_arr.shape[0]
    s1 = sim_arr.shape[0]

    if r1 != 10 or s1 != 10:
        raise TypeError('Shape is not 10', real_arr.shape, sim_arr.shape)

    real_click_num = int(np.sum(real_arr))
    array_query_id_to_real_clicks_arr[q_id][real_click_num] += 1

    sim_click_num = int(np.sum(sim_arr))
    array_query_id_to_simulated_clicks_arr[q_id][sim_click_num] += 1

    array_query_id_to_second_simple_clicks_arr[q_id][0] += 1

    array_query_id_to_weight[q_id] += 1

    line_num += 1


def calculate_for_frequency(q_ids, name):
    print('Number of line:', line_num)
    print('end reading info from files -> start calculate local KL_div')
    sum_div_sim = 0
    sum_div_first = 0
    sum_div_second = 0
    n = 0

    assert len(q_ids) == len(np.unique(q_ids))

    print('calculate local KL-div by Sessions for ' + name + ' queries')
    print('Number of unique query:', len(q_ids))

    for q_id in q_ids:
        # real
        p = array_query_id_to_real_clicks_arr[q_id]

        # simulated
        q1 = array_query_id_to_simulated_clicks_arr[q_id]
        w = array_query_id_to_weight[q_id]
        print(p)

        p[p == 0] = 1e-5
        q1[q1 == 0] = 1e-5

        div_sim = scipy.stats.entropy(p, q1)

        sum_div_sim += div_sim * w

        #### first simple
        q2 = array_query_id_to_first_simple_clicks_arr[q_id]

        q2[q2 == 0] = 1e-5

        div_first = scipy.stats.entropy(p, q2)
        print('div_first', div_first)

        sum_div_first += div_first * w

        #### second simple
        q3 = array_query_id_to_second_simple_clicks_arr[q_id]

        q3[q3 == 0] = 1e-5

        div_second = scipy.stats.entropy(p, q3)
        print('div_second', div_second)

        sum_div_second += div_second * w

        n += w

    print('n', n)
    print('Mean local KL div simulated', sum_div_sim / n)
    print('Mean local KL div first', sum_div_first / n)
    print('Mean local KL div second', sum_div_second / n)


all_q_ids = np.where(q_id_to_count != 0)[0]
rare_q_ids = np.where((q_id_to_count >= 1) & (q_id_to_count < 10))[0]
torso_q_ids = np.where((q_id_to_count >= 10) & (q_id_to_count < 100))[0]
frequent_q_ids = np.where((q_id_to_count >= 100))[0]

calculate_for_frequency(all_q_ids, "all")
calculate_for_frequency(rare_q_ids, "rare")
calculate_for_frequency(torso_q_ids, "torso")
calculate_for_frequency(frequent_q_ids, "frequent")
